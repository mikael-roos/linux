#!/usr/bin/env bash

echo "Hello world!"


echo "There are $# arguments"
echo "The arguments are: '$*'"
echo "The first value is '$0'"
echo "The second value is '$1'"


#read 
#echo $REPLY


MESSAGE="Hello World"
VALUE=42

printf "%s and the answer is %d\n" \
    "$MESSAGE" \
    $VALUE



LISTING=$( ls )
printf "%s" "$LISTING"



function main {
    local arg1="$1"
    local arg2=$2

    printf "1: %s\n2: %s\n" "$arg1" "$arg2"
}

main "This is arg 1" "Arg 2"



printf "The script was called with %d parameters.\n" $#
printf "The script executable is '%s'\n" "$0"

if (( $# == 1 ))
then
    printf "The first parameter is '%s'\n" "$1"
else
    printf "There is not one argument sent to the script.\n"
fi



for i in $( ls ); do
    echo item: "$i"
done

for i in `seq 1 10`;
do
    echo $i
done



COUNTER=0
while [  $COUNTER -lt 10 ]; do
    echo The counter is $COUNTER
    let COUNTER=COUNTER+1
done

COUNTER=0
while (( COUNTER < 10 )); do
    echo The counter is $COUNTER
    let COUNTER=COUNTER+1
done



exit 0
