![What now?](public/img/linux-what-now.png)

Weekly log - 2023 period 3
==========================

This is an overview of the course setup and what happens week by week.

[[_TOC_]]

<!--
TODO

* Chek out thse on Docker:
    * https://roadmap.sh/docker
    * https://courses.devopsdirective.com/docker-beginner-to-pro

IMPROVE

* Läsanvisningar, vad kan studenterna läsa?
    * Artiklar som exemplifierar hur man kan jobba
    * Specifika setups

EXTEND

* Build your own favorite Linux server setup for a specific purpose
* Use docker to setup and configure som specific servers (same as above)?
* Use docker containers to setup more advanced cluster of servers
* Configure firewall and nftables, fail2ban, other security issues, perhaps read the article on security and then choose some parts and implement them
* Use containers to work more with the network services
    * Check network traffic
    * Firewall
    * Login tries
    * ssh login with keys
    * vpn
* Use container with a compromised system and let the students find out what was compromised
    * Logs
    * access & error logs for services
    * lastlogin
    * whologin
-->

Prepare yourself
--------------------------

1. Introduction to the Unix part of the course
    * [Slides: Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro-23/slide.html)

1. Get a bash terminal
    * Linux - you already know where you have it
    * Mac - you have it, start "Terminal"
    * Windows - choose a terminal to work with
        * [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
        * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
        * [Cygwin](https://www.cygwin.com/)

1. You may want to view the video "[Use various bash terminals on Windows (Git Bash, Cygwin, Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

1. We will use [Git](https://git-scm.com/) & GitLab, so you should install it locally.
    * You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".

1. There will be opportunities to learn how to work with [Docker](https://docs.docker.com/get-docker/), do install it locally and use it in the assignments.

1. We will use [VirtualBox](https://www.virtualbox.org/), do install it on your system. It will be used in the assignments.

1. To install your own Linux server. You might want to use VirtualBox, dual boot, your own hardware, or perhaps a Raspberry Pi or your own server in the cloud - feel free to do so and start preparing it already now.



Assignments and deadlines
--------------------------

* [A01](A01.md) start 25/1, submit code and lab report 17/2.

* [A02](A02.md) start 8/2, submit lab report & recorded presentation 17/3.

* Re-exam (try2) Friday May 26, 2023.
* Rest-exam (try3) Friday August 25, 2023.



Literature
--------------------------

The following literature will be used during the course.

1. The website [LinuxCommand.org](http://linuxcommand.org/) has what we need to get going with the shell and with shell programming.

    1. Book "[The Linux Command Line](http://linuxcommand.org/tlcl.php)" that can be downloaded as pdf from the above website.

1. Book "[The Debian Administrator's Handbook](https://debian-handbook.info/get/)" is available online and as PDF.



Resources
--------------------------

These resources are ueful as a reference.

* [Explain the shell command to me](https://explainshell.com/).

* [Bash Reference Manual](https://www.gnu.org/software/bash/manual/)

* [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)



01: Linux shell and shell scripting
--------------------------

About Unix & Linux in general, the Linux terminal, the shell and shell scripting with bash.

**Teacher activities**

Lectures with slides:

* [Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro-23/slide.html)
* [History and Architecture](https://mikael-roos.gitlab.io/linux/lecture/history-and-architecture/slide.html)
<!--
Replaced by walkthrough of E02.
* [The shell and the commands](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-commands/slide.html)
-->
* [Shell scripting with Bash](https://mikael-roos.gitlab.io/linux/lecture/shell-scripting/slide.html)

**Read & Study**

* The article [GNU Bash & The UNIX Philosophy](https://ryapric.github.io/2019/06/30/bash-unix.html)

* Review the book "The Linux Command Line", you might cherry pick your readings but the 7 first chapters are of most importance to support your needs to solve the assignment.

**Work**

* Work through the exercise "[Execute the example programs in a Docker container](./E01.md)".
* Work through the exercise "[Use the shell and commands](./E02.md)".
* Work through the exercise "[Write shell scripts](./E03.md)".

**Assignment**

* [A01](./A01.md) is presented and you can start working on it.



02: Linux server
--------------------------

Install and configure the Linux server.

How to work with a Linux server. We will review the Debian Linux distribution and see how it works as an organisation, releases, software packages, installation and configuration and how to work with it. This provides you with an overview of how such a Linux distribution might work and review some details on the system.

**Teacher activities**

Lecture with slides:

* [Debian System Administration](https://mikael-roos.gitlab.io/linux/lecture/debian-system-administration/slide.html)
<!--
* [The shell and the system](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-system/slide.html)
-->

**Read & Study**

Read and work through:

* Appendix B: Short Remedial Course in the "The Debian Administrator's Handbook" to learn about the basics of the parts of the Debian system (this is useful to fullfill A02).

**Work**

* Work through the exercise "[Get access to and use a virtual server](./E04.md)".
* Work through the exercise "[Using the shell and the system](./E05.md)".

**Assignments**

* A01 should be submitted next week.
* [A02](A02.md) is presented and you can start working on it.



03: Linux services
--------------------------

Various services and configurations on the Linux server.

Install and configure services in a Linux server.

About docker images and containers with Docker and Docker-compose.

**Teacher activities**

Lecture with slides:

* [The shell and the services](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-services/slide.html)
* [Docker and docker-compose](https://mikael-roos.gitlab.io/linux/lecture/docker-and-docker-compose/slide.html)

<!--
* Perhaps improve the services slides also, sometimes the slides are just to many, perhaps add more stories around it? 
* improve the docker slides, more images, less slides, use examples on how it works, other rather abstract to learn?
-->

**Work**

* Work through the exercise "[Using the shell and manage services](./E06.md)".
* Work through the exercise "[Build a docker image and execute a container](./E07.md)".

**Read & study**

* [How To Secure A Linux Server](https://github.com/imthenachoman/How-To-Secure-A-Linux-Server)


<!--
* Läs om Docker versus Virtualization
https://github.com/CS-LNU-Learning-Objects/web-application-architecture/blob/master/containers.md

Läs om olika typer av specialicerade Linux installationer.

* NAS
* Home server
* Media server
* Intelligent home server
* Own web server
* Own cloud
* Own virtualisation server
-->

**Assignments**

* Work with A02.



Later
--------------------------

A02 should be submitted.
