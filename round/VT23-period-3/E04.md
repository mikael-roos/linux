---
revision:
    "2023-02-08": "(A, mos) First version."
---
Get access to and use a virtual server
==========================

Get going with a Ubuntu server in CSCloud.

[[_TOC_]]


<!--
TODO

* (Material/video on how to work with the CSCloud?)

-->



Precondition
--------------------------

You can handle a bash terminal with some basic commands.

The computer you have access to is publicly reachable over the Internet. You are only allowed to use this machine for the purposes described in this course. You are not allowed to use the machine for private projects.

The SSH-key you have received (through GitLab) should be considered a secret, and it is important that you do not send it to a third party or show the key in any online streams.


<!--
Prepare
--------------------------
-->


VPN (Virtual Private Network)
--------------------------

To protect the machines from attacks, CSCloud is hidden behind a firewall. To be able to access CSCloud from outside of the university network you will need to connect through a VPN. Make sure you connect through the VPN when trying to connect to CSCloud.

[Guide for connecting to the VPN](https://serviceportalen.lnu.se/sv-se/article/1448381)



Get going
--------------------------

You have the login details in GitLab in a repo called secrets. Clone it and use the details to login to the CSCloud server. 

Start by cloning the repo with the secrets.

```
git clone git@gitlab.lnu.se:1dv721/student/<student acronym>/secrets.git
cd secrets
```

Ensure that you have the following in the repo.

* IP address
* Domain name
* User name
* File with ssh key

Download the ssh key file to your local system, you will need it when you try to login to the server.

If you need help, then there is a [video showing how to do it](https://www.youtube.com/watch?v=hb0fWnC8BxI).



Access to the server
--------------------------

Before you try to login, ensure that you have access to the server by pinging it.

```text
$ ping myserver.se
PING myserver.se (225.89.96.6) 56(84) bytes of data.
64 bytes from 225.89.96.6 (225.89.96.6): icmp_seq=1 ttl=54 time=24.3 ms
64 bytes from 225.89.96.6 (225.89.96.6): icmp_seq=2 ttl=54 time=23.4 ms
```

You can also use the command traceroute to see what path is taken to reach your server.

```
$ traceroute myserver.se
... output from the command...
```

At the final line of the output you should see the ip address of your own server and how long time it too to reach it.

If you have no access to the server, try to login to the VPN and see if that provides access to the server.



Login using ssh
--------------------------

You can now use the ssh key to login into your system using ssh.

```
ssh -i keyfile ubuntu@server.some.where
```

You can logout from the system as this.

```
exit
```



Update the system
--------------------------

When you have logged onto the system you can start to update it to ensure that your system is up to date with the latest versions installed.

```text
sudo apt update
sudo apt upgrade -y
```



Install Git and clone a repo
--------------------------

Install git using apt.

Clone the repo "resources", you find it at the same place as the secrets repo. You can also use your "secrets" repo for this same purpose if you do not have a "resources" repo. 

Clone it both to the virtual host, and locally to your desktop. Now you have a repo that you can use to share files between your local desktop and the virtual server.

You might need to setup the ssh key between the virtual servern and GitLab. Do that.

This repo will also get you a possibility to keep a backup of the files you need to setup and configure your virtual server. A backup is really good if you for some reaseon need to re-install your virtual server from scratch.



Terminal multiplexor tmux
--------------------------

The nice thing with a terminal multiplexor is that it can be your "desktop environment" for a server. You can start a tmux session and then leave (logout) the server. When you login to the server the next time you can attach to the existing tmux session and just continue to work.

Quickly [read about tmux](https://github.com/tmux/tmux/wiki).

Install tmux using apt.

Add a configuration file for tmux that uses `ctrl a` instead of the default `ctrl b` key sequence.

Save the following file as `~/.tmux.conf`.

```text
unbind-key C-b
set-option -g prefix C-a
bind-key C-a send-prefix
```

Now you can start and try to learn tmux. It is quite useful when working with a server.



Summary
--------------------------

You now have access to your virtual host and you can start working with it.
