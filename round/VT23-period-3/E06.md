---
revision:
    "2023-02-21": "(A, mos) First version."
---
Using the shell and manage services
==========================

Practice using the shell and some useful commands for working with services as an administrator. Install and configure the services, start, stop and monitor them.

[[_TOC_]]


<!--
TODO

* How to create your own service under systemd?
* What are the most common used services, do a walkthrough of them?
* Add exercise to create more services, for example using docker?
-->



Precondition
--------------------------

You have worked through the exercise "[Using the shell and the system](./E05.md)".

You have reviewed the slides "[The shell and the services](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-services/slide.html)".

The commands in this exercise is tried out on a Ubuntu server. You might need to adapt if you are running this on another type of system.



Prepare
--------------------------

Login to your virtual host (or another Linux server) to carry out the exercise.

Ensure to add your own log file where you can store important commands that you might want to remember later on.



Explain the shell command
--------------------------

The web service [explainshell.com](https://explainshell.com/explain) can be used to exaplin details of the commands used in this exercise, use it as a complement to read the man pages.



systemd
--------------------------

> Which is the first process started when you boot your system?

The service manager systemd is a system and service manager for Linux operating systems. When run as first process on boot (as PID 1), it acts as init system that brings up and maintains userspace services.

Separate instances are started for logged-in users to start their services.

Check out its man page.

```
man systemd
```



Show details on the systemd process
--------------------------

Use `ps` to find details on the systemd process.

```
$ ps -fp 1                                                             
UID          PID    PPID  C STIME TTY          TIME CMD                
root           1       0  0 jan09 ?        01:13:01 /sbin/init splash  
```

Use the manpage of ps to find out the meaning of the following.

* UID
* PID
* PPID

Continue to find the same details on the systemd process started for your own user.

Find the process systemd owned by you.

```
ps -u <user> | grep systemd
```

See which PPID the process has and how it was started.

```
ps -fp <pid>
```

Verify that you understand the concept of how PPID links the processes.



List and monitor services
--------------------------

> What services are installed at your system and what are their status?

The tool systemctl control the systemd system and service manager. It may be used to introspect and control the state of the "systemd" system and service
manager.

Briefly review the manpage for systemctl.

```
man 1 systemctl
```

This command shows you all the loaded and activly running services mananged by systemd.

```
systemctl --type=service --state=running
```

You can see an overview of the services with this command.

```
systemctl status
```

Add the service name to show specific details on one service.

```
$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2023-01-09 18:12:07 CET; 1 month 12 days ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 958 (sshd)
      Tasks: 1 (limit: 37580)
     Memory: 1.4M
        CPU: 13ms
     CGroup: /system.slice/ssh.service
             └─958 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"
```



Crontab services
--------------------------

> How to automate actions happening at regular times and intervals?

This is an example on one of the services managed by systemd.

You should in a previous exercise have tried to create a entry in the crontab and executed that at a regular interval. This is performed by the cron daemon which is a service in the background.

You can control the status of cron daemon/service and perhaps will you also see some log details on the processes that cron has started.

```
systemctl status cron
```

Check out the man pages for both cron and crontab, just so you know what details they contain.



Install and manage a server
--------------------------

> How to install and manage a server?

To learn this we are going to install the [web server Nginx](https://www.nginx.com/) as a service and start it up to listening on port 8080.

The [documentation for Nginx open source server](https://nginx.org/en/docs/) provides us with details for the process of installing and configuration.



### Install

The packages for Nginx is not included in Ubuntu so we need to add their packet repository and download it from there.

Start by ensuring that your system is up to date.

```
sudo apt update && sudo apt upgrade -y
```

Then follow the Nginx instructions on how to install it. While installing it, note to your self how the installation procedure does the following.

* Verifies the integrity using GPG keys
* Adds an external repository

You can verify that the Nginx package repository is included in your system.

```
$ apt policy | grep nginx

900 http://nginx.org/packages/ubuntu jammy/nginx amd64 Packages
     release o=nginx,a=stable,n=jammy,l=nginx,c=nginx,b=amd64
     origin nginx.org
```

You should be able to see tha actual file like this.

```
$ ls -l /etc/apt/sources.list.d/nginx.list
-rw-r--r-- 1 root root 107 feb 21 16:02 /etc/apt/sources.list.d/nginx.list
```

Open the file to learn how it looks like.

When it all is setup it is only the installation left to do.

```
apt install nginx
```

Before you proceed, check what files were added to your system through the package nginx.

```
dpkg -L nginx
```

<!--
You can also use tha package ``apt-file` to see what files will be installed by a package, before installing it.

```
# Run apt-file update if this is the first time using the command
apt-file nginx
```

It might be interesting to know what files were actually installed, before or after doing the installation.

-->

It might be interesting to know what files were actually.


### Status

The nginx server can be managed by systemctl and we can start by checking its status.

```
$ systemctl status nginx
○ nginx.service - nginx - high performance web server
     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: https://nginx.org/en/docs/
```

Ok, it seems to be loaded but not active.



### Start and stop

We can start the server using systemctl (use sudo to run it).

```
systemctl start nginx
```

Check the status of the service.

```
systemctl status nginx
```

Try to stop it and then start it again. YOu will need to sudo the start and stop.

```
systemctl stop nginx
systemctl status nginx
systemctl start nginx
```

You can verify that the server is actually working by opening a web browser to show it at `http://localhost`.

It can look like this.

![nginx welcome](img/nginx_welcome.png)



### Configure

The default configuration file is available in `/etc/nginx/` as `nginx.conf` and through `conf.d/`. Open the configuration resources to review their content.

By default the Nginx web server default location is at `/usr/share/nginx/html`. Verify what the directory contain, it should be some web file.

To ensure that you can add web files, create another file `hello.html` in that directory and put the following content into it.

```
Hello World
```

You should now be able to open the file through the url `https://localhost/hello.html`.

It can look like this.

![hello html](img/hello-html.png)

Ok, it seems to work.



### Logging

Log files for services are usually stored below `/var/log`.

By default, the nginx access log is located at `/var/log/nginx/access.log`, verify that it contains the log entries you just made.



### Reload and restart

Lets make a change in the configuration file and then reload it, without restarting the service. It should just reload its configuration.

We need to update the configuration file with the following entry. You can find this setting in the file `/etc/nginx/conf.d/default.conf`.

```
server {
    listen 9080;
}
```

Now make nginx to reload its configuration (use sudo).

```
systemctl reload nginx
```

Check the status of the service and you might find trace in the log that it was reloaded.

Verify that the service now listens on port 9080 using your web browser and the url `http://localhost:9080`

It can look like this.

![nginx port](img/nginx-port.png)

Doing reload is a nicer way than restarting the server. Reload just reloades the configuration and tries to keep the service upp all the time. Doing a restart might involve a short period of downtime.



### Summary

This was the process of installing, configuring, start, stop, restart and reload a service controller by systemctl.

You might have read in the manual for Nginx that all of these actions can be done through using the actual nginx software also. What systemctl provides is a friendly and unified way to access all services on the system.



Ports mapping to services
--------------------------

> What standard services are using what port numbers?

The file `etc/services` is a plain ASCII file providing a mapping between human-friendly textual names for internet services, and their underlying assigned port numbers and protocol types.

Use that file to learn the name of the services using the ports 22, 80, 443 and 3306.



Ports used by services
--------------------------

> What ports are used by the services?

Use the command `ss` or `netcat` to find the ports used by a specific service.

You can use the following command to find what process are listening to some port.

```
ss -ltup

# or limit the output

ss -ltup | grep <port>
ss -ltup | grep <name>
```

Chack if you can find ssh and niginx in the output.

Do not forget to visit the man page to check up the meaning op the options sent to the command.



Open ports and firewall
--------------------------

> What ports are open to the world on my machine?

You can use the tool `nmap` to scan your host to view the open ports it has. Review the man page for details and try it out using the following command.

```
sudo nmap -v -sT localhost
```

Even if the port is open it might still be blocked by a firewall or any other software/hardware. One way to further invesitage this is to check if there is any rules for the firewall.

Check the rules that applies to the firewall, if any.

```
sudo iptables -S
```

There is a frontend command `ufw` that may help you configure the firewall rules in the iptables. You can check its status like this.

```
systemctl status ufw
# or
sudo ufw status
```

Use the man pages to learn more on iptables and ufw and how the relate to each other.



Summary
--------------------------

You now have practiced the shell together with administrating the services in the system.
