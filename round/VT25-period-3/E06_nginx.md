---
revision:
    "2025-02-23": "(B, mos) First release."
    "2024-02-21": "(A, mos) First draft, copy from E6."
---
Install and configure a service (Nginx)
==========================

Practice using the shell and commands to install, configure and maintain a service. We are to use the Nginx web server for this example.

[[_TOC_]]


<!--
TODO

* Show how to configure cgi on nginx.
* Add configuration of https (or do in the security exercise)
* Add example of the error log from nginx
* Configure reverse proxy, configure database as exercise?
    * Update A2 to use nginx configuration like this?

-->



Precondition
--------------------------

You have worked through the exercise "[Using the shell and manage services](./E06.md)".

The commands in this exercise is tried out on a Ubuntu server. You might need to adapt if you are running this on another type of system.



Prepare
--------------------------

Login to your virtual host (or another Linux server) to carry out the exercise.

Ensure to add your own log file where you can store important commands that you might want to remember later on.



Install and manage a service
--------------------------

The main task is now to learn "How to install and manage a service".

To learn this we are going to install the [web server Nginx](https://www.nginx.com/) as a service and start it up to listening on port 80.

The [documentation for Nginx open source server](https://nginx.org/en/docs/) provides us with details for the process of installing and configuration.



Install
--------------------------

The latest packages for Nginx is not included in Ubuntu so we need to add their packet repository and download it from there.

Start by ensuring that your system is up to date.

```bash
sudo apt update && sudo apt upgrade -y
```

Then follow the [Nginx instructions on how to install it](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/). Select the installation option "Installing a Prebuilt Ubuntu Package from an Ubuntu Repository".

While installing it, note to your self how the installation procedure does the following.

* Verifies the integrity using GPG keys
* Adds an external repository

You can verify that the Nginx package repository is included in your system.

```bash
$ apt policy | grep nginx

900 http://nginx.org/packages/ubuntu jammy/nginx amd64 Packages
     release o=nginx,a=stable,n=jammy,l=nginx,c=nginx,b=amd64
     origin nginx.org
```

You should be able to see the actual file like this.

```bash
$ ls -l /etc/apt/sources.list.d/nginx.list
-rw-r--r-- 1 root root 107 feb 21 16:02 /etc/apt/sources.list.d/nginx.list
```

Open the file to learn how it looks like.

You can use the tool `apt-file` to see what files will be installed by a package, before installing it.

```bash
# Run apt-file update if this is the first time using the command
apt-file update

# Check what files are included in nginx package
apt-file list nginx
```

It might be interesting to know what files were actually installed, before or after doing the installation.

Now you can install the pacakge onto your system.

```bash
apt install nginx
```

Before you proceed, check what files were added to your system through the package nginx.

```bash
dpkg -L nginx
```

It might be interesting to know what files were actually added to your system.



Status
--------------------------

The nginx server can be managed by systemctl and we can start by checking its status.

```bash
$ systemctl status nginx
○ nginx.service - nginx - high performance web server
     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: https://nginx.org/en/docs/
```

Ok, it seems to be loaded but not active.



Start and stop
--------------------------

We can start the server using systemctl (use sudo to run it).

```bash
systemctl start nginx
```

Check the status of the service.

```bash
systemctl status nginx
```

Try to stop it and then start it again. YOu will need to sudo the start and stop.

```bash
systemctl stop nginx
systemctl status nginx
systemctl start nginx
```

You can verify that the server is actually working by opening a web browser to show it at `http://localhost` or check it using `curl http://localhost`.

It can look like this when you check in your web browser.

![nginx welcome](img/nginx_welcome.png)

_Figure. The welcome page for a Nginx installation._

You now have the web server up and running and you now how to manage it.



Logs if you get into trouble
--------------------------

If you see a message like this, something is trouble.

> Job for nginx.service failed because the control process exited with error code.
> See "systemctl status nginx.service" and "journalctl -xeu nginx.service" for details.

You can then use any of the to utilities to check the logs.

```bash
systemctl status nginx.service
journalctl -xeu nginx.service
```


Configure
--------------------------

The default configuration file is available in `/etc/nginx/` as `nginx.conf` and through `conf.d/`. Open the configuration resources to review their content.

The default website is enabled as `sites-enabled/default`. Look into that file to explore where the default webfiles are.

To ensure that you can add web files, create another file `hello.html` in that directory and put the following content into it.

```html
Hello World
```

You should now be able to open the file through the url `http://localhost/hello.html`.

It can look like this.

![hello html](img/hello-html.png)

_Figure. Your own created HTML page saying "Hello"._

Ok, it seems to work.



Logging
--------------------------

Log files for services are usually stored below `/var/log`.

By default, the nginx access log is located at `/var/log/nginx/access.log`, verify that it contains the log entries you just made.

The access log looks like this, it logs each access to the web server.

```bash
$ cat /var/log/nginx/access.log 
::1 - - [23/Feb/2025:22:33:10 +0100] "GET / HTTP/1.1" 200 615 "-" "curl/8.5.0"
::1 - - [23/Feb/2025:22:33:46 +0100] "GET /hello.html HTTP/1.1" 404 162 "-" "curl/8.5.0"
::1 - - [23/Feb/2025:22:34:00 +0100] "GET / HTTP/1.1" 200 615 "-" "curl/8.5.0"
::1 - - [23/Feb/2025:22:34:04 +0100] "GET /hello.html HTTP/1.1" 404 162 "-" "curl/8.5.0"
::1 - - [23/Feb/2025:22:38:09 +0100] "GET /hello.html HTTP/1.1" 200 13 "-" "curl/8.5.0"
```

You can find the default `error.log` in the same directory.

<!--
* make an example of the error log
-->


Reload and restart
--------------------------

Lets make a change in the configuration file and then reload it, without restarting the service. It should just reload its configuration.

We need to update the configuration file with the following entry. You can find this setting in the file `/etc/nginx/sites-available/default.conf`.

```json
server {
    listen 9080;
}
```

Now make nginx to reload its configuration (use sudo).

```bash
systemctl reload nginx
```

Check the status of the service and you might find trace in the log that it was reloaded.

Verify that the service now listens on port 9080 using your web browser and the url `http://localhost:9080`

You can try it locally using curl.

```bash
curl http://localhost:9080
```

It can look like this.

![nginx port](img/nginx-port.png)

_Figure. Running the server on another port means I need to change the url to access the page._

Doing reload is a nicer way than restarting the server. Reload just reloades the configuration and tries to keep the service upp all the time. Doing a restart might involve a short period of downtime.

Now change it back to port 80 again and reload the server and verify that it changed the port.



<!--
Configure CGI
--------------------------

_Show how to configure cgi on nginx._

-->



<!--

Configure https
--------------------------

Egen övning?



Configure reverse proxy
--------------------------

Borde vara egen övning.


-->



Summary
--------------------------

This was the process of installing, configuring, start, stop, restart and reload a service controller by systemctl.

You might have read in the manual for Nginx that all of these actions can be done through using the actual nginx software also. What systemctl provides is a friendly and unified way to access all services on the system.
