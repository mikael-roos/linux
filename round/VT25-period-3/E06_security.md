---
revision:
    "2025-02-23": "(B, mos) First released version."
    "2024-03-04": "(A, mos) First version."
---
Securing the system with protective measures
==========================

When the server is up it can be further protected to minimize the risks that a potential attacker can gain access to information about the system and that way learn more on what voulerabilities exists on the specific system.

[[_TOC_]]


<!--
TODO

* Aligna dessa övningar med föreläsning om säkerhet
    * What ar ethe basics idea behind securing a system?

* Check iptables/nftables
    * Add example of outputting from nft command
* Work with ufw
* Add fail2ban
* psad
* aide

* https, Lets encrypt and https

* Inga filer, kan lösas med config?
    * GET /.git/config

More security configs on the nginx/apache service?

* Own exercise only focusing on the security services?
  * Protect a system
  * Find flaws in a system
* Inspect the logs to view what happened on the system?

* Configure firewall and nftables, fail2ban, other security issues, perhaps read the article on security and then choose some parts and implement them
* Use containers to work more with the network services
    * Check network traffic
    * Firewall
    * Login tries
    * ssh login with keys
    * ssh tunnel, is that a thing for a server like this?
    * vpn, setup to the server, for fun?
* Use container with a compromised system and let the students find out what was compromised
    * Logs
    * access & error logs for services
    * lastlogin
    * whologin
-->

<!--
# Basic security

* Keep your system updated (automatically)
* Provide no info on your system
* Limit the services you run and the ports being open
* Know what services are run on the server and what ports are open
* Know what files you have on the server (and if they change)
* Ensure a valid authentication for the server
* Prevent other from brute force authentication
* Monitor logs to detect abuse
* Keep your secrets safe



# Supervision: Prevention, Detection, Deterrence

Is my system compromised?

* Monitoring Logs with - `logcheck`
* Monitor activity in realtime using - `top`
* Detect changes `dpkg --verify`

-->


Precondition
--------------------------

You have access to your production server with an Ubuntu installation.

You have worked through the exercise "[Install and configure a service (Nginx)](./E06_nginx.md)".

The commands in this exercise is tried out on a Ubuntu server. You might need to adapt if you are running this on another type of system.



Prepare
--------------------------

Login to your production server to carry out the exercise.

Ensure to add your own log file where you can store important commands that you might want to remember later on.



Open ports and firewall
--------------------------

> What ports are open to the world on my machine?

You can use the tool `nmap` to scan your host to view the open ports it has. Review the man page for details and try it out using the following command.

```bash
sudo nmap -v -sT localhost
```

Even if the port is open it might still be blocked by a firewall or any other software/hardware. One way to further investiage this is to check if there is any rules for the firewall.

Check the rules that applies to the firewall, if any.

```bash
sudo iptables -S
```

There is a frontend command `ufw` that may help you configure the firewall rules in the iptables. You can check its status like this.

```bash
systemctl status ufw
# or
sudo ufw status
```

You can get more details on the rules, like this.

```bash
sudo ufw status numbered
# or
sudo ufw status verbose
```

Use the man pages to learn more on iptables and ufw and how the relate to each other.

PS. In this exercise, avoid doing any configuration of the firewall, you might shut yourself out of the system



Continously upgrades of the system
--------------------------

The tool "Unattended Upgrades" handles updates and what to do when updating the system, for example.

* Which types of updates to install automatically (e.g., security updates, recommended updates).
* How to handle package blacklisting (preventing specific packages from being upgraded).
* Whether to automatically reboot the system if necessary (e.g., after a kernel update).
* Email notifications for administrators.

The configuration file where to do this is in `/etc/apt/apt.conf.d/50unattended-upgrades`. Quickly review its content.

This tool is executed by the timers. IN the file `/etc/apt/apt.conf.d/20auto-upgrades` can you enable or disable the automatic updates and upgrades.



Nginx/Apache: Avoid giving out details on the system
--------------------------

A default installation of the web servers Nginx and Apache will be verbose with details on the server and the service itself.

It is good practice to avoid letting anyone outside of your system know what services you run and their versions.

You can check what details you get from your webserver headers like this.

```bash
curl --head http://localhost
```

If you can see any details about the actual server, then you are at risk. You need to configure to not display that.

In nginx, add the following to your nginx configuration `/etc/nginx/nginx.conf`.

```
server_tokens off;
```

In Apache you edit the file `/etc/apache2/conf-enabled/security.conf`.

```
ServerTokens Prod
ServerSignature Off
```

After you have edit the configuration files, reload the service and verify what headers is sent out.



Summary
--------------------------

You have now performed measures in trying to add the protection and security level of your server and you have learned a bit more on details on securing your system.
