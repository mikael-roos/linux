---
revision:
    "2025-01-10": "(A, mos) First version."
---
![What now?](public/img/linux-what-now.png)

Weekly log - 2025 period 3
==========================

This is an overview of the course setup and what happens week by week.

[[_TOC_]]

<!--
TODO

2025
*

2024
* Bestäm att WSL är minsta gemensamma nämnare för labbmiljön. 
* Visa hur man instalelrar docker i labbmiljön så vi kan använda det.
* Introducera docker container redan dag 1, bra för testning, utveckling och validering av bash-scripten.
* Förbättra läsanvisningarna in i boken?
* Bestäm att alla skall ha en Linux tillhanda, via virtualbox eller annat. Eller använd CSCloud redan från dag 1?
* Alla skall ha koppling till gitlab och repon för A1 och A2 skall clonas/skapas därifrån

* En övning om awk, sed, grep, find och mer kraftfulla unix utilities?
* En övning om att konfigurera en server säkert, kan utföras inifrån docker container
* Fixa README till alla föreläsningar, förbättra läsanvisningar
* Fixa läsanvisningar i övningarna?
* Förbättra container-delen, använd även LXD och kanske övning?
* https://roadmap.sh/docker Roadmap on how to learn docker
* Bygg om docker-föreläsningen i stil med:
    * https://courses.devopsdirective.com/docker-beginner-to-pro

* Build your own favorite Linux server setup for a specific purpose
    * Hmm, koppla till cscloud?
    * Eller bara styr upp fler saker som skall konfigureras på servern?
    * Kanske sedan använda samma konfiguration i docker-container?
-->

Course round
--------------------------

This course round is in study period 3, January to March.

These are the dates for this course round.

| What  | Date       | Week | Explanaition     |
|:------|:----------:|:----:|------------------|
| Start | 2025-01-20 |   4  | Course starts    | 
| End   | 2025-03-30 |  13  | Course ends      |
| Try 2 | 2025-05-23 |  21  | Re-examination   |
| Try 3 | 2025-08-29 |  35  | Rest-examination |



Course elements
--------------------------

This is an overview of the different course elements and when they take place in the course.

| Course element | w01 | w02 | w03 | w04 | w05 | w06 | w07 | w08 | w09 | w10 |
|:--------|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 01: Linux shell and shell scripting ||🟩|||||||||
| Assignment 01 - Shell scripting with Bash (A01) ||🟩|🟩|🟩|🟩|🟩|||||
| 02: Linux server ||||🟩|||||||
| 03: Linux services ||||||🟩|||||
| 04: Container technologies ||||||||🟩|||
| Assignment 02 - Work with Linux (A02) ||||||🟩|🟩|🟩|🟩|🟩|



### Assignment deadlines

This is a general overview of the deadlines to submit the assignments.

| Activity | Deadline | Second deadline | Third deadline |
|:---------|:--------:|:---------------:|:--------------:|
| A01 | Friday course week 06 | Try 2 | Try 3 |
| A02 | Friday course week 10 | Try 2 | Try 3 |
| Try 2 | ~End of May |
| Try 3 | ~End of August |

For actual dates you need to look in the course platform or above in the details of the course round.



Prepare yourself
--------------------------

1. Introduction to the Unix part of the course
    * [Slides: Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro/slide_2025.html)

1. Get a bash terminal
    * Linux - you already know where you have it
    * Mac - you have it, start "Terminal"
    * Windows - choose a terminal to work with
        * **Recommendation** [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
        * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
        * [Cygwin](https://www.cygwin.com/)

1. You may want to view the video "[Use various bash terminals on Windows (Git Bash, Cygwin, Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

1. We will use [Git](https://git-scm.com/) & GitLab, so you should install it locally.
    * You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".

1. There will be opportunities to learn how to work with [Docker](https://docs.docker.com/get-docker/), do install it locally and use it in the assignments.

1. We will use [VirtualBox](https://www.virtualbox.org/), do install it on your system. It will be used in the assignments.

1. You will install your own Linux server. You might want to use VirtualBox, dual boot, your own hardware, or perhaps a Raspberry Pi or your own server in the cloud - feel free to do so and start preparing it already now.



Literature
--------------------------

The following main literature will be used during the course.

1. The website [LinuxCommand.org](http://linuxcommand.org/) has what we need to get going with the shell and with shell programming.

    1. Book "[The Linux Command Line](http://linuxcommand.org/tlcl.php)" that can be downloaded as pdf from the above website.

1. Book "[The Debian Administrator's Handbook](https://debian-handbook.info/get/)" is available online and as PDF.



Resources
--------------------------

These resources are useful as a reference.

* [Explain the shell command to me](https://explainshell.com/).

* [Bash Reference Manual](https://www.gnu.org/software/bash/manual/)

* [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)



01: Linux shell and shell scripting
--------------------------

<!--
TODO

* Hur förhålla sig till labbmiljön? Måste det tydliggöras hur det skall isntalelras i denna första veckan? Troligen nej, kursen startar veckan innan och de kan göra förberedelser då.
* Fixa README till föreläsningarna, lägg till instruktion till varje slide.

-->

About Unix & Linux in general, the Linux terminal, the shell and shell scripting with bash.

**Teacher activities**

Lectures with slides:

* [Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro/slide_2025.html)
* [History and Architecture](https://mikael-roos.gitlab.io/linux/lecture/history-and-architecture/slide.html)
* [Shell scripting with Bash](https://mikael-roos.gitlab.io/linux/lecture/shell-scripting/slide.html)

**Read & Study**

* The article [GNU Bash & The UNIX Philosophy](https://ryapric.github.io/2019/06/30/bash-unix.html)

* Review the (free) book "The Linux Command Line", you might cherry pick your readings but the 7 first chapters are of most importance to support your needs to solve the assignment.

* Resources for bash programming.
    * [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)
    * [Bash Reference Manual](https://www.gnu.org/software/bash/manual/)

**Work**

* Work through the exercise "[Execute the example programs in a Docker container](./E01.md)".
* Work through the exercise "[Use the shell and commands](./E02.md)".
* Work through the exercise "[Write shell scripts](./E03.md)".

**Assignment**

* [A01](./A01.md) is presented and you can start working on it.



02: Linux server
--------------------------

Install and configure the Linux server.

How to work with a Linux server. We will review the Debian Linux distribution and see how it works as an organisation, releases, software packages, installation and configuration and how to work with it. This provides you with an overview of how such a Linux distribution might work and review some details on the system.

**Teacher activities**

Lecture with slides:

* [Debian System Administration](https://mikael-roos.gitlab.io/linux/lecture/debian-system-administration/slide.html)

<!--
* Perhaps split into two lectures of 30 min or so. Its still a bit long and hard to have the attention up. Would be easier if it was two (or three) different topics.
-->

**Read & Study**

Read and work through:

* Appendix B: Short Remedial Course in the "The Debian Administrator's Handbook" to learn about the basics of the parts of the Debian system (this is useful to fullfill A02).

<!--
* Article on Debian project & Ubuntu project
* Howto setup your own home server, find article to read
* Perhaps Rsapberry PII
* Article on other related distros
-->

**Work**

* Work through the exercise "[Get access to and use a virtual server](./E04.md)".
<!--
* Work through the exercise "[Using the shell and the utilities](./E05_utilities.md)".
-->
* Work through the exercise "[Using the shell and the system](./E05_system.md)".

**Assignments**

* A01 should be submitted next week.
* [A02](A02.md) is presented and you can start working on it.



03: Linux services
--------------------------

Various services and configurations on the Linux server.

Install and configure services in a Linux server.

**Teacher activities**

Lecture with slides:

* [The shell and the services](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-services/slide.html)

<!--
* Security threats and tools would be more intresting, how to secure a server as a single slideshow
* Split into two lectures, one about services and one about securing your server.
* Tools to check if the system can be breaken from an external source
-->

**Work**

Work through the following exercises.

*  [Using the shell and manage services](./E06.md).
*  [Install and configure a service (Nginx)](./E06_nginx.md).
*  [Securing the system with protective measures](./E06_security.md).

<!--
* Enhance exercise focus on security and build slides around it
    * Make the exercise match the slides and vice versa
-->

**Read & study**

* [How To Secure A Linux Server](https://github.com/imthenachoman/How-To-Secure-A-Linux-Server), review the content and select parts to read.

<!--
Read on how to secure system and how to detect intrusions.

Läs om olika typer av specialicerade Linux installationer.

* NAS
* Home server
* Media server
* Intelligent home server
* Own web server
* Own cloud
* Own virtualisation server
-->

**Assignments**

* Work with A02.



04: Container technologies
--------------------------

Introduction to container technologies in general.

About docker images and containers with Docker and Docker-compose.

**Teacher activities**

Lecture with slides:

* [Docker and docker-compose](https://mikael-roos.gitlab.io/linux/lecture/docker-and-docker-compose/slide.html)

**Work**

* Work through the exercise "[Build a docker image and execute a container](./E07.md)".

**Read & study**

* Read through the [overview of Docker](https://docs.docker.com/get-started/overview/)
* About installing Docker to your machine
    * [Docker Desktop](https://docs.docker.com/desktop/)
    * [Docker Engine](https://docs.docker.com/engine/)
* [About the tool `docker-compose`](https://docs.docker.com/compose/)


**Assignments**

* Work with A02 and prepare to submit it.
