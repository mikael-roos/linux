---
revision:
    "2023-02-08": "(A, mos) First version."
---
Using the shell and the system
==========================

Practice using the shell and some useful commands for working with system as a server where you have administrator rights.

[[_TOC_]]


<!--
TODO

* Enhance Network
* Work through to add more examples and usages and explanations
* Review more enhancements down in the file, might take an hour to work through it.

* mounting files systems (/etc/fstab)
* Kanske omstarter och single user, boot faser, hur systemet bootar upp
* How to work as root?

timedatectl status
sudo timedatectl set-ntp true

-->



Precondition
--------------------------

You have worked through the exercise "[Get access to and use a virtual server](./E04.md)".

You have worked through the exercise "[Using the shell and the utilities](./E05_utilities.md)".

You have revied the slides "[The shell and the system](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-system/slide.html)".

Use the man-pages to learn the basics on each command you execute.



Prepare
--------------------------

Login to your virtual host.

In your repo "resources", add a logfile where you can store important commands that you might want to remember later on.



su to root
--------------------------

The root user is usually the only user with administrator rights, or "full rights" to do whatever with the system.

When you login to the system you are usually an ordinary user and when you want to do actions needing administrator privilegies then you need to switch to the root user.

It is common to use the command `su` to execute commands as root.

```text
su <command>
```

You can check if your user is allowed to su.

<!--
# check your own user (does not work since needing the password)
sudo -v
-->

```text
# check some user
sudo -l -U <user_name>
```

To be able to su you need to be configured as a user that can use su. This can be configured in the file `/etc/sudoers` and in the directory`/etc/sudoers.d`.

One way to add a new user to be able to execute commands as sudo is to add the user to the sudo group.

```text
sudo usermod -aG sudo <user_name>
```

You can start a shell as root using the sudo command, however, that is seldom needed.

```text
sudo bash
whoami
exit
```



Check your timzone
--------------------------

Verify the time and timezone used by the machine.

```
timedatectl
```

If you need to change it you run the following command.

```
sudo dpkg-reconfigure tzdata
```

You can run the above command just to verify how it looks like.



Identify your system
--------------------------

When you first take over a system there are a few things you can start doing to get aquainted with your system.

Find out what the following files and commands can do for you.

* /etc/debian_version
* /etc/apt/sources.list
* /etc/apt/sources.list.d
* /usr/local/
* apt-show-versions
* debsums (check checksums of installed files)
* cruft, cruft-ng (find files not part of package, might take some time to run)

All the tools above are ways to check the integrity of the server to see what is installed on it. For example, before upgrading a server such information could be important.



Update the system
--------------------------

View what packages are installed on the system.

```
sudo apt list
```

Check if any packages need to be updated and simulate an upgrade.

```
sudo apt upgrade -s
```

Check in the configuration files below `/etc/apt` where all your repositories are.

Show details on any installed package using its cached information. For example check the package name `curl` or `cowsay`.

```
apt-cache show <package name>
```

Ensure that your system is up to date with the latest versions installed.

```text
sudo apt update && sudo apt upgrade -y
```


<!--
Automatic security updates
--------------------------

Explain how that is done?
-->



Job control
--------------------------

Job control can help you work at the terminal. You can start practising this by creating a bash-script sleeps for 60 seconds and then prints a message. Create such a script and name it `sleep.bash` and make it executable.

Now start the script and then press `ctrl-c` to interrupt it.

Try to see what happens if you press `ctrl-s` in the terminal. Can you do anything? Press `ctrl-q` to undo it.

Start the script again and then press `ctrl-z` to stop it. Then enter `bg` to put the script running into the background.

Start another job (script) and add a `&` after it to send it directly to the background.

You can list all jobs you have by the command `jobs`.

You can send any of the jobs to the foreground using `fg %<id>`. Send it to the background again.

You can killa job using `kill %<id>`.

You can break the program execution with `ctrl-c` on a program you have started in the foreground of the shell.

If the program does not shutdown, then you can try `ctrl-z` to stop its execution.



Processes
--------------------------

Use the command `top` to find what processes uses the most resources currently. You can also try `htop` and see if you fancy that instead.

Start a job of your `sleep.bash` into the background. Try finding it with `pgrep sleep`.

Kill it using `kill`.

Use `ps` together with some options to get a nice listing of all the processes on your system and show who owns the process.

<!--
You can list all `signals` that you can send to a process.

Show what options to use with ps.
-->

Figure out what the program `pkill` can do for you. It makes it easier to kill a certain application.



User and groups
--------------------------

Print details on the `users` (run the command) that are logged into your system.

Use `last` to see a list on who has last logged on to your system.

Check who you are using `whoami` and what groups you are in using `groups`.

Can you find details on your own user in the files `/etc/passwd` and `/etc/groups`.

Use `man -k users` and `man -k groups` to learn more on commands related.

Find the command on how to create a new user. Run it to create a new user. Verify that you can login as a new user. Then find out how you can prevent that user from loggin in to the system and execute to avoid the user from loggin in.

Check what files were generated into the home directory of the new user, including the hidden ones.

Can you figure out what the files `/etc/shadow` and `/etc/nsswich.conf` do with respect to users and groups?



<!--
Name service
--------------------------
-->



Filesystem
--------------------------

Figure out what the command `df` does and try to figure out how much free space you have in your filesystem.

Check the content of the file `/etc/fstab` and see if you can see a match with the output of the `df`.

Use the command `mount` to see its output and use the manpage to understand what it says.

The command `file` is quite useful to figure out details on a file in the filesystem.

If you want more information on a file or directory, then try out `stat`. Try to learn what a inode is.

Links are pointers to existings files (and directories). Use `ln` and `ln -s` to create links to some files (and directories). What is the difference on a hard and a soft link?

The command `du -s` can be used to calculate the size of a particular directory. Can you use it to find which of the directories in the root that contains the most data?


<!--
This should perhaps be in the "utilities" exercise.

* Permission
    * user, group, other
* Rights
    * read, write, execute
* Setuid, setgid
* chown, chgrp
* chmod
    * 755, rwxr-xr-x (r=4, w=2, x=1), usr-group-other

```
-rw-r--r-- 1 mos mos  164 Jan 25 10:19 README.md
drwxr-xr-x 2 mos mos 4.0K Jan 25 20:38 bin/
```


* [Standard](https://wiki.debian.org/FilesystemHierarchyStandard)
* /
* Automount
* df, du, mount
* /etc/fstab

* ext4 (fat, ntfs, mfl)
* mkfs

-->



Memory
--------------------------

Try out the following three commands that prints out details on the memory of the system. Which command seems to be the easiest to understand?

* `free -m`
* `vmstat -s`
* `cat /proc/meminfo`

Use the man pages to learn more on the commands if needed.

<!--
Add more details on what happens with the commands.
-->



Devices
--------------------------

Use the command `hwinfo` to find out details on your hardware.

Use the command `lspci` to list all PCI devices of your system.

Use the command `lsusb` to check all connected USB ports to your system.

Use `lsdev | less` to display (a lot) information on the installed devices on your system.

<!--
Add more details on what happens with the commands.
-->



Network
--------------------------

<!-- This can be improved to exaplin more and show examples -->

Try to find some commands that let you find out the ipadress of your system.

Can you also find some command that can provide a list of all open ports on your system?

What purpose has the files `/etc/hosts` and `/etc/resolv.conf` on your system?



<!--
Domain name service
--------------------------

nslookup
traceroute

-->



Logfiles
--------------------------

<!-- Enhance this with more examples -->

The directory `/var/log` contains logfiles. Can you figure out which logfile the command `lastlog` uses?

Select three of the logfiles in the directory and inspect their content and look up in the man pages what their purpose are.



Crontab
--------------------------

Crontab can execute commands at a certain time, and repeat the commands.

Try to create a crontab entry that creates a empty file in some directory once each minute. Remove the crontab entry when you are sure that the command works.

<!-- Show how its done -->



Keep your system up to date
--------------------------

<!-- ERewrite this and explain how its done -->

When you are responsible for a system you want it to be updated with the lates security fixes.

Perhaps you can use the crontab to keep your system up to date. Check what they say in the following question.

* https://askubuntu.com/questions/1142077/download-updates-with-crontab



<!--
Generate a report of the system status
--------------------------

Exercise to check all parts of the system, mem, cpu, etc -> shell script to print a report (perhaps in text or in html).
-->



Summary
--------------------------

You now have practiced the shell together with inspecting the system. Hopefylly have you got some insight into how the system works and how you can inspect its details.
