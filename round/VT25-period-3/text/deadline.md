The first deadline is coming up, tomorrow night. If you reach it - be satisfied with yourself. It's good practice to reach a deadline. It is one of the proofs one can present of doing good work.

I will grade the incoming submissions "within 15 work days".

**Can you extend a deadline "for me, please"?**

> I have a real good excuse, can you extend the deadline for me?

No. That is unfair to the ones making the deadline and takes away the pressure and satisfaction of actually meeting a deadline.

I am usually not comfortable with changing deadlines, nor extending them. It does not depend on your excuse, it depends on my view of deadlines.

**What is the consequence of missing a deadline?**
> What happens if I submit on Sunday or Monday or next Friday?
Missing the deadline means you are sorted in another hat (or pile). All submissions in that hat are not guaranteed to be graded within 15 working days. They are graded later, or when time gets free.

**When is later?**
It depends. But I will at least "clean up" all submissions at the end of the course, even the late ones. I might grade it sooner, you can not know, and neither do I.

We also have the try 2 and try 3 when you can submit later and after this course occasion ends.

**What are your recommendations?**
Proceed with your work. Do your best. Submit when you are done, even if it is after the deadline.
Cross your thumbs and hope for the best that grading comes sooner, but expect the worst if you miss the deadline.

I hope it is clear enough so you can make your plans.

1. Plan A is to meet the deadlines.
2. Create a plan B if you need to, the info above should present the guidelines for plan B.
3. Ask questions if unclear.