![What now?](public/img/linux-what-now.png)

Weekly log - 2022 period 3
==========================

This is an overview of the course setup and what happens week by week.

[[_TOC_]]



Prepare yourself
--------------------------

1. Introduction to the Unix part of the course (recorded video)
    * [Slides Course Intro](https://mikael-roos.gitlab.io/linux/lecture/course-intro-22/slide.html)

1. Get a bash terminal
    * Linux - you already know where you have it
    * Mac - you have it, start "Terminal"
    * Windows - choose a terminal to work with
        * [Cygwin](https://www.cygwin.com/)
        * [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
        * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))

1. You may want to view the video "[Use various bash terminals on Windows (Git Bash, Cygwin, Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

1. We will use [Git](https://git-scm.com/) & GitLab, so you should install it locally.
    * You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".

1. There will be opportunities to learn how to work with [Docker](https://docs.docker.com/get-docker/), do install it locally if you want to take this opportunity.

1. We will use [VirtualBox](https://www.virtualbox.org/), do try to install it on your system. This is the assignment 02.

1. To install your own Linux server. You might want to use VirtualBox, dual boot, your own hardware, or perhaps a Raspberry Pi or your own server in the cloud - feel free to do so and start preparing it already now.



Assignments and deadlines
--------------------------

* [A01](A01.md) start 26/1, submit code and lab report 9/2.

* [A02](A02.md) start 9/2, submit lab report & recorded presentation <s>9/3</s> 23/3.

Dates for re-examination is yet to be scheduled.



Literature
--------------------------

The following literature will be used during the course.

1. The website [LinuxCommand.org](http://linuxcommand.org/) has what we need to get going with the shell and with shell programming.

1. Book "[The Linux Command Line](http://linuxcommand.org/tlcl.php)" that can be downloaded as pdf from the above website.

1. Book "[The Debian Administrator's Handbook](https://debian-handbook.info/get/)" is available online and as PDF.



Resources
--------------------------

* [Explain the shell command to me](https://explainshell.com/).

* The "[Bash Reference Manual](https://www.gnu.org/software/bash/manual/)"

* [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)



01: First meeting
--------------------------

About Unix & Linux in general, the Linux terminal, the shell and shell scripting with bash.

Read:

* The article [GNU Bash & The UNIX Philosophy](https://ryapric.github.io/2019/06/30/bash-unix.html)

* The Linux Command Line, you might cherry pick your readings but the 7 first chapters are of most importance to support your needs to solve the assignment.

Lectures with slides:

* [History and Architecture](https://mikael-roos.gitlab.io/linux/lecture/history-and-architecture/slide.html)
* [The Shell](https://mikael-roos.gitlab.io/linux/lecture/the-shell/slide.html)
* [Shell scripting with Bash](https://mikael-roos.gitlab.io/linux/lecture/shell-scripting/slide.html)

<!--
* Create an exercise where you can work with the shell, job control and commands.
* Create an exercise in writing a bash script.
-->

A01 is presented and you can start working on it.



02: Second meeting
--------------------------

How to work with a Linux server. We will reiew the Debian Linux distribution and see how it works as an organisation, releases, software packages, installation and configuration and how to work with it. This provides you with an overview of how such a Linux distribution might work and review some details on the system.

Lecture with slides:

* [Debian System Administration](https://mikael-roos.gitlab.io/linux/lecture/debian-system-administration/slide.html)

Read and work through:

* Appendix B: Short Remedial Course in the "The Debian Administrator's Handbook" to learn about the basics of the parts of the Debian system (this is useful to fullfill A02).

Work:

* Assure that you can login to the CSCloud server you have been givven to you on GitLab.

<!--
* Add exercise where you can work on your own with some useful commands.
* Perhaps reduce the slides with examples on commands?
-->

A01 should be submitted.

<s>A02 is presented and you can start working on it.</s> A02 will be presented within a few days.



03: Third meeting
--------------------------

Install and configure services in a Linux server.

About images and containers with Docker and Docker-compose.

Lecture with slides:

* [Docker and docker-compose](https://mikael-roos.gitlab.io/linux/lecture/docker-and-docker-compose/slide.html)

The exercise repo is this repo and it can be cloned like this. The repo contains cgi-files that can be used from the containers in the `docker-compose.yaml` file.

```
git clone https://gitlab.com/mikael-roos/linux.git
```

Work with A02.




Later
--------------------------

A02 should be submitted.
