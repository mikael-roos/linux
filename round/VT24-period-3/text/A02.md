Hi,
I have reviewed your report and video and graded it according to the assignment specification:
* https://gitlab.com/mikael-roos/linux/-/blob/master/round/VT23-period-3/A02.md
During the grading a look at the video and I read the report. In that work I look in particular for a few things, some of these are:
* Did the work match the requirements.
* The perceived quality of the video presentation.
* Well written text in general.
* Use of images with the text.
* The blend of real text versus images.
* Structure of the document, template, table of content, references, appendix.
* The size and length of the document.
* The "real text" with its structure, content and reasoning.
The bits and pieces builds a feeling that leads to a grade.
Let me know if you have any thoughts or questions.
//Mikael
