![What now?](public/img/linux-what-now.png)

Weekly log - 2024 period 3
==========================

This is an overview of the course setup and what happens week by week.

[[_TOC_]]

<!--
TODO

2024
* Bestäm att WSL är minsta gemensamma nämnare för labbmiljön. 
* Visa hur man instalelrar docker i labbmiljön så vi kan använda det.
* Introducera docker container redan dag 1, bra för testning, utveckling och validering av bash-scripten.
* Förbättra läsanvisningarna in i boken?
* Bestäm att alla skall ha en Linux tillhanda, via virtualbox eller annat. Eller använd CSCloud redan från dag 1?
* Alla skall ha koppling till gitlab och repon för A1 och A2 skall clonas/skapas därifrån

* En övning om awk, sed, grep, find och mer kraftfulla unix utilities?
* En övning om att konfigurera en server säkert, kan utföras inifrån docker container
* Fixa README till alla föreläsningar, förbättra läsanvisningar
* Fixa läsanvisningar i övningarna?
* Förbättra container-delen, använd även LXD och kanske övning?
* https://roadmap.sh/docker Roadmap on how to learn docker
* Bygg om docker-föreläsningen i stil med:
    * https://courses.devopsdirective.com/docker-beginner-to-pro

* Build your own favorite Linux server setup for a specific purpose
    * Hmm, koppla till cscloud?
    * Eller bara styr upp fler saker som skall konfigureras på servern?
    * Kanske sedan använda samma konfiguration i docker-container?
-->

Prepare yourself
--------------------------

1. Introduction to the Unix part of the course
    * [Slides: Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro-24/slide.html)

1. Get a bash terminal
    * Linux - you already know where you have it
    * Mac - you have it, start "Terminal"
    * Windows - choose a terminal to work with
        * **Recommendation** [WSL/WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
        * Git bash (the terminal is included with the [Git installation](https://git-scm.com/))
        * [Cygwin](https://www.cygwin.com/)

1. You may want to view the video "[Use various bash terminals on Windows (Git Bash, Cygwin, Debian/WSL2)](https://www.youtube.com/watch?v=kialYZs6Oyc&list=PLEtyhUSKTK3gHj087mUjPfXyqMvSy2Rwz&index=1)".

1. We will use [Git](https://git-scm.com/) & GitLab, so you should install it locally.
    * You may want to view the video "[Install Git from the installation program on Windows 10 and configure it](https://www.youtube.com/watch?v=02u7ao7uK5k&list=PLEtyhUSKTK3iTFcdLANJq0TkKo246XAlv&index=1)".

1. There will be opportunities to learn how to work with [Docker](https://docs.docker.com/get-docker/), do install it locally and use it in the assignments.

1. We will use [VirtualBox](https://www.virtualbox.org/), do install it on your system. It will be used in the assignments.

1. To install your own Linux server. You might want to use VirtualBox, dual boot, your own hardware, or perhaps a Raspberry Pi or your own server in the cloud - feel free to do so and start preparing it already now.



Assignments and deadlines
--------------------------

* [A01](./A01.md) start 24/1, submit code and lab report 17/2.

* [A02](./A02.md) start 7/2, submit lab report & recorded presentation 24/3.

* Re-exam (try2) Friday May 26.
* Rest-exam (try3) Friday August 25.



Literature
--------------------------

The following main literature will be used during the course.

1. The website [LinuxCommand.org](http://linuxcommand.org/) has what we need to get going with the shell and with shell programming.

    1. Book "[The Linux Command Line](http://linuxcommand.org/tlcl.php)" that can be downloaded as pdf from the above website.

1. Book "[The Debian Administrator's Handbook](https://debian-handbook.info/get/)" is available online and as PDF.



Resources
--------------------------

These resources are useful as a reference.

* [Explain the shell command to me](https://explainshell.com/).

* [Bash Reference Manual](https://www.gnu.org/software/bash/manual/)

* [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)



01: Linux shell and shell scripting
--------------------------

About Unix & Linux in general, the Linux terminal, the shell and shell scripting with bash.

**Teacher activities**

Lectures with slides:

* [Course introduction](https://mikael-roos.gitlab.io/linux/lecture/course-intro-24/slide.html)
* [History and Architecture](https://mikael-roos.gitlab.io/linux/lecture/history-and-architecture/slide.html)
* [Shell scripting with Bash](https://mikael-roos.gitlab.io/linux/lecture/shell-scripting/slide.html)

**Read & Study**

* The article [GNU Bash & The UNIX Philosophy](https://ryapric.github.io/2019/06/30/bash-unix.html)

* Review the book "The Linux Command Line", you might cherry pick your readings but the 7 first chapters are of most importance to support your needs to solve the assignment.

* Resources for bash programming.
    * [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/)
    * [Bash Reference Manual](https://www.gnu.org/software/bash/manual/)

**Work**

* Work through the exercise "[Execute the example programs in a Docker container](./E01.md)".
* Work through the exercise "[Use the shell and commands](./E02.md)".
* Work through the exercise "[Write shell scripts](./E03.md)".

**Assignment**

* [A01](./A01.md) is presented and you can start working on it.



02: Linux server
--------------------------

Install and configure the Linux server.

How to work with a Linux server. We will review the Debian Linux distribution and see how it works as an organisation, releases, software packages, installation and configuration and how to work with it. This provides you with an overview of how such a Linux distribution might work and review some details on the system.

**Teacher activities**

Lecture with slides:

<!--
Föreläsningen behöver mer översyn för att ge den flyt och innehåll.
Tex ta bort delen med installation och berätta mer om hur man jobbar som root.
Kanske omstarter och single user, boot faser, hur systemet bootar upp
-->

* [Debian System Administration](https://mikael-roos.gitlab.io/linux/lecture/debian-system-administration/slide.html)

**Read & Study**

Read and work through:

* Appendix B: Short Remedial Course in the "The Debian Administrator's Handbook" to learn about the basics of the parts of the Debian system (this is useful to fullfill A02).

<!--
* Article on Debian project & Ubuntu project
* Howto setup your own home server, find article to read
-->

**Work**

* Work through the exercise "[Get access to and use a virtual server](./E04.md)".
* Work through the exercise "[Using the shell and the system](./E05.md)".

**Assignments**

* A01 should be submitted next week.
* [A02](A02.md) is presented and you can start working on it.



03: Linux services
--------------------------

Various services and configurations on the Linux server.

Install and configure services in a Linux server.

**Teacher activities**

Lecture with slides:

* [The shell and the services](https://mikael-roos.gitlab.io/linux/lecture/the-shell-and-the-services/slide.html)

<!--
* Perhaps improve the services slides also, sometimes the slides are just to many, perhaps add more stories around it? 
* Automation of daily management tasks
* Security threats and tools
-->

**Work**

* Work through the exercise "[Using the shell and manage services](./E06.md)".

<!--
* Split exercise into two separate, on with installing nginx (and database, and cgi?)
    * the other focus on services
-->

**Read & study**

* [How To Secure A Linux Server](https://github.com/imthenachoman/How-To-Secure-A-Linux-Server), review the content and select parts to read.


<!--
Läs om olika typer av specialicerade Linux installationer.

* NAS
* Home server
* Media server
* Intelligent home server
* Own web server
* Own cloud
* Own virtualisation server
-->

**Assignments**

* Work with A02.



04: Container technologies
--------------------------

Introduction to container technologies in general.

About docker images and containers with Docker and Docker-compose.

**Teacher activities**

Lecture with slides:

* [Docker and docker-compose](https://mikael-roos.gitlab.io/linux/lecture/docker-and-docker-compose/slide.html)

**Work**

* Work through the exercise "[Build a docker image and execute a container](./E07.md)".

**Read & study**

* Read through the [overview of Docker](https://docs.docker.com/get-started/overview/)
* About installing Docker to your machine
    * [Docker Desktop](https://docs.docker.com/desktop/)
    * [Docker Engine](https://docs.docker.com/engine/)
* [About the tool `docker-compose`](https://docs.docker.com/compose/)


**Assignments**

* Work with A02.



Later
--------------------------

A02 should be submitted.
