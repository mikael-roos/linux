---
revision:
    "2024-03-04": "(A, mos) First version."
---
Securing the system with protective measures
==========================

When the server is up it can be further protected to minimize the risks that a potential attacker can gain access to information about the system and that way learn more on what voulerabilities exists on the specific system.

[[_TOC_]]


<!--
TODO

* Check iptables/nftables
* Work with ufw
* Add fail2ban
* psad
* aide

* Own exercise only focusing on the security services?
* Inspect the logs to view what happened on the system?

* Configure firewall and nftables, fail2ban, other security issues, perhaps read the article on security and then choose some parts and implement them
* Use containers to work more with the network services
    * Check network traffic
    * Firewall
    * Login tries
    * ssh login with keys
    * ssh tunnel
    * vpn
* Use container with a compromised system and let the students find out what was compromised
    * Logs
    * access & error logs for services
    * lastlogin
    * whologin
-->



Precondition
--------------------------

You have access to your production server with an Ubuntu installation.

<!--
You have worked through the exercise "[Install and configure a service (Nginx)](./E06_nginx.md)".
-->

The commands in this exercise is tried out on a Ubuntu server. You might need to adapt if you are running this on another type of system.



Prepare
--------------------------

Login to your production server to carry out the exercise.

Ensure to add your own log file where you can store important commands that you might want to remember later on.



Nginx/Apache: Avoid giving out details on the system
--------------------------

A default installation of the web servers Nginx and Apache will be verbose with details on the server and the service itself.

<!--
Jag får sårbarhetrapporter från IT-avd.
Ibland är det så många att det är svår att se vilka som är riktiga hot eller inte.
För att minska antal larm som är falska sårbarheter i nginx och apache servrar som körs i Ubuntu, vill jag att alla gör ändringar så att webbservern inte svarar med versionsnummer m.m.
Dessa inställningar bör man ju ändå göra i en produktionsmiljö, så det är lika bra att lära sig.
Man kan själv testa sin webbservrar med curl --head <dnsnamn eller IP>
t.ex  curl --head cscloud9-999.lnu.se
Om man får svar exempelvis Server: nginx/1.18.0 (Ubuntu) är inte konfigurationen gjord.
Kan du se till att det går ut till studenter att de ska göra följande ändringar:
Nginx:
Ändra i /etc/nginx/nginx.conf till:
server_tokens off;
Och sedan systemctl restart nginx.service
Apache:
Ändra i /etc/apache2/conf-enabled/security.conf till:
ServerTokens Prod
ServerSignature Off
Och sedan systemctl restart apache2.service


-- 
Inga filer, kan lösas med config?
GET /.git/config

--
update av ubuntu, via systemd, installeras automatiskt och bootar om

-->



<!--
Open ports and firewall
--------------------------

> What ports are open to the world on my machine?

You can use the tool `nmap` to scan your host to view the open ports it has. Review the man page for details and try it out using the following command.

```
sudo nmap -v -sT localhost
```

Even if the port is open it might still be blocked by a firewall or any other software/hardware. One way to further invesitage this is to check if there is any rules for the firewall.

Check the rules that applies to the firewall, if any.

```
sudo iptables -S
```

There is a frontend command `ufw` that may help you configure the firewall rules in the iptables. You can check its status like this.

```
systemctl status ufw
# or
sudo ufw status
```

Use the man pages to learn more on iptables and ufw and how the relate to each other.
-->



Summary
--------------------------

You have now performed measures in trying to add the protection and security level of your server.
