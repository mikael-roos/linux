---
revision:
    "2023-02-21": "(A, mos) First version."
---
Docker containers with network and persistent volumes
==========================

<!--
Learn how to create a docker image using a Dockerfile. Start the image as a container using docker-compose. Mount the local filesystem onto the container and use the container as your development environment.
-->

[[_TOC_]]


<!--
TODO

* Docker with several hosts working together
* Persistent volumes with a database
* Network with docker (or create extra exercise for a multi container setup)
-->



Precondition
--------------------------

You have worked through the exercise "[Using the shell and manage services](./E06.md)".

You have reviewed the slides "[Docker and docker-compose](https://mikael-roos.gitlab.io/linux/lecture/docker-and-docker-compose/slide.html)".

You have access to the linux repo where the example files used in this exercise are.

You have docker and docker-compose installed locally on your system.



Prepare
--------------------------

In the linux repo, create a directory `me/docker` where we can start working and save your files. Go to that directory.



Create the configuration file `docker-compose.yaml`
--------------------------

This is a sample `docker-compose.yaml` that will start a Linux server for you. Create the file and save the following configuration into it.

```
version: "3"

services:
    debian:
        image: debian:stable-slim

    ubuntu:
        image: ubuntu:latest

    alpine:
        image: alpine:latest
```

You can try it out like this to start a terminal in the container for each Linux image.

```
docker-compose run debian bash
docker-compose run ubuntu bash
docker-compose run alpine sh
```

Try it out with the image from ubuntu and the one from alpine (use `sh` as the shell instead ob `bash`).



Official images on Docker Hub
--------------------------

Do visit Docker Hub and review the docs for the images.

* [Debian](https://hub.docker.com/_/debian/)
* [Ubuntu](https://hub.docker.com/_/ubuntu)
* [Alpine](https://hub.docker.com/_/alpine)



Build your own image
--------------------------

You can add your own image on top of another image. For example, you can install some tools that are not available in the default image, making it more custom and useful for your application.

Create a sub directory and name it `my` as an example label of your own service. Add an empty file `Dockerfile` to it.

```
mkdir my
touch my/Dockerfile
```

In the `my/Dockerfile` you can now add instructions on how to build your own image. This is a sample on how to do it.

```
FROM ubuntu

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        cowsay \
    && rm -rf /var/lib/apt/lists/*

ENV PATH="/usr/games:${PATH}"

WORKDIR /app
```

Ok, now we update the `docker-compose.yml` with a new service that will use our Dockerfile to build itself.

```
docker-compose build my
```

Now we can start the container and check that it contains what is needed.

```
docker-compose run my bash
```

It should look something like this.

```
root@d3dbb181514f:/app# cowsay "Hello my image"
 ________________
< Hello my image >
 ----------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

The intention with this container is to attach to it and execute commands in the isolation of the container.



Run the container and attach a shell
--------------------------

Lets create a container running a server/service.

Copy all you did above, but create a complete new service as `mys` instead.

In the Dockerfile, add so that the package `apache2` is included, that is the Apache web server.

In the bottom of the `mys/Dockerfile`, add the following command to start the apache server.

```
EXPOSE 80
CMD ["apachectl", "-D", "FOREGROUND"]
```

Now build the service `mys`.

Start like this.

```
docker-compose up mys
```

Perhaps you already have some service locally on your machine that occupy the port 80, then add the following to map the container port to another local port in your `docker-compose.yml`.

```
services:
    mys:
        build: ./mys
        ports:
            - "8088:80"
```

Verify that you can connect to the apache server in the container by opening a web browser and connect to the url `http://localhost:8088`.



Start in background and shutdown
--------------------------

You can start a container as detached running in the background.

```
docker-compose up -d mys
```

Later you can take it down like this.

```
docker-compose down
```



Connect to a running container
--------------------------

You can connect to a detached container by its service name.

First start up the mys conatiner as detached.

```
docker-compose up -d mys
```

Then execute a command in that running container.

```
docker-compose exec mys pwd
docker-compose exec mys cowsay "Hello"
```

You can also connect to the running container and attach a bash terminal where you can work interactivly.

```
docker-compose exec mys bash
```

Now, when you have opened a bash in the container, try to use the command `apachectl status` to see the status of the Apache server.

```
apachectl status
```

You will most likely need to add the package `lynx` to your image to be able to execute `apachectl`. Just add it and rebuild the image and start the container again and try once more.



Show running containers
--------------------------

You can use `docker-compose ps` to show current running containers.

```
$ docker-compose ps
    Name                  Command               State                  Ports                
--------------------------------------------------------------------------------------------
docker_mys_1   /bin/sh -c apachectl -D FO ...   Up      0.0.0.0:8088->80/tcp,:::8088->80/tcp
```

You can also see what ports are being mapped by the container.



Configure the web server using COPY
--------------------------

The apache server has its default configuration file in `/etc/apache2/sites-enabled/000-default.conf`. Sometimes you want to edit that file and make it a part of the image or the container.

You can check its content by copying it from the container and saving it as a local file in your `mys/` directory like this.

```
docker-compose exec mys cat /etc/apache2/sites-enabled/000-default.conf > mys/000-default.conf
```

You can now edit the config file, just to verify that it works. You can for example set the `ServerName mys.somewhere.org` as part of the configuration.

Edit your local copy of the config file and add this line as the first line in the file.

```
ServerName mys.somewhere.org 
```

Now you need to update the `mys/Dockerfile` and copy the local file into the image during the build process.

```
COPY 000-default.conf /etc/apache2/sites-enabled/
```

You can verify that it works by rebuilding the server and starting it up (not as detached). You should NOT get the following error when starting the container.

```
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.20.0.2. Set the 'ServerName' directive globally to suppress this message
```

This is how you can build an adapted image from a set of local source build files.

<!--
You can now edit the config file, just to verify that it works. You can for example edit the port that the web server listens to, change it from 80 to 8080.

Edit your local copy of the config file, it is this line you need to change.

```
<VirtualHost *:80>
```

Do not forget that you now need to also change the setting in `Dockerfile` for the port you are exposing.

You also need to update the `docker-compose.yml` to reflect the local port you are mapping to.
-->



Mount local volumes into container
--------------------------

Sometimes you want to mount local files into the container. That could be one option instead of using `COPY` to make the files a part of the image.

Mounting local files can be done in the `docker-compose.yml` file. Try it like this.

Create a local directory `html` in the same directory as the `docker-compose.yml` file. Add a file `hello.html` into that directory and let it contain just a message saying "Hello World".

Now mount that directory and replace the current web root in the container. you do that by adding the following update to the `docker-compose.yml` file.

```
    mys:
        build: ./mys
        ports:
            - "8088:80"
        volumes:
            - ./html:/var/www/html
```

The part we add are the `volumes` section. It mounts the local directory `./html` onto the containers directory `/var/www/html`.

You can verify that it works using your browser or curl. There is no need to rebuild the image, but you need to restart the container.

```
$ curl localhost:8088/hello.html
Hello world!
```

Mounting local files onto the container enables it to act as a development environment for you. You edit the local files and they are accessed through the container.

<!--
Final exercise
--------------------------

Now when you have control of the process, try to do the following to show to yourself that you can do it.

* Update the configuration file and change the directory from where it 
-->

<!--
Configure the web server to allow cgi scripts
--------------------------
-->



Summary
--------------------------

You now have practiced how to create a docker image with its own content and how to work with it by using docker-compose.
