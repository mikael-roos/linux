---
revision:
    "2024-02-21": "(A, mos) First draft, copy from E6."
---
Install and configure a service (Nginx)
==========================

Practice using the shell and commands to install, configure and maintain a service. We are to use the Nginx web server for this example.

[[_TOC_]]


<!--
TODO

* Check what ports are open on the system, before and after the installation.
* Show how to configure cgi on nginx.

-->



Precondition
--------------------------

You have worked through the exercise "[Using the shell and manage services](./E06.md)".

The commands in this exercise is tried out on a Ubuntu server. You might need to adapt if you are running this on another type of system.



Prepare
--------------------------

Login to your virtual host (or another Linux server) to carry out the exercise.

Ensure to add your own log file where you can store important commands that you might want to remember later on.



Install and manage a service
--------------------------

> How to install and manage a service?

To learn this we are going to install the [web server Nginx](https://www.nginx.com/) as a service and start it up to listening on port 8080.

The [documentation for Nginx open source server](https://nginx.org/en/docs/) provides us with details for the process of installing and configuration.



Install
--------------------------

The packages for Nginx is not included in Ubuntu so we need to add their packet repository and download it from there.

Start by ensuring that your system is up to date.

```
sudo apt update && sudo apt upgrade -y
```

Then follow the Nginx instructions on how to install it. While installing it, note to your self how the installation procedure does the following.

* Verifies the integrity using GPG keys
* Adds an external repository

You can verify that the Nginx package repository is included in your system.

```
$ apt policy | grep nginx

900 http://nginx.org/packages/ubuntu jammy/nginx amd64 Packages
     release o=nginx,a=stable,n=jammy,l=nginx,c=nginx,b=amd64
     origin nginx.org
```

You should be able to see tha actual file like this.

```
$ ls -l /etc/apt/sources.list.d/nginx.list
-rw-r--r-- 1 root root 107 feb 21 16:02 /etc/apt/sources.list.d/nginx.list
```

Open the file to learn how it looks like.

When it all is setup it is only the installation left to do.

```
apt install nginx
```

Before you proceed, check what files were added to your system through the package nginx.

```
dpkg -L nginx
```

It might be interesting to know what files were actually added to your system.

<!--
You can also use tha package ``apt-file` to see what files will be installed by a package, before installing it.

```
# Run apt-file update if this is the first time using the command
apt-file nginx
```

It might be interesting to know what files were actually installed, before or after doing the installation.

-->




Status
--------------------------

The nginx server can be managed by systemctl and we can start by checking its status.

```
$ systemctl status nginx
○ nginx.service - nginx - high performance web server
     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: https://nginx.org/en/docs/
```

Ok, it seems to be loaded but not active.



Start and stop
--------------------------

We can start the server using systemctl (use sudo to run it).

```
systemctl start nginx
```

Check the status of the service.

```
systemctl status nginx
```

Try to stop it and then start it again. YOu will need to sudo the start and stop.

```
systemctl stop nginx
systemctl status nginx
systemctl start nginx
```

You can verify that the server is actually working by opening a web browser to show it at `http://localhost` or check it using `curl http://localhost`.

It can look like this when you check in your web browser.

![nginx welcome](img/nginx_welcome.png)



Configure
--------------------------

The default configuration file is available in `/etc/nginx/` as `nginx.conf` and through `conf.d/`. Open the configuration resources to review their content.

By default the Nginx web server default location is at `/usr/share/nginx/html`. Verify what the directory contain, it should be some web file.

To ensure that you can add web files, create another file `hello.html` in that directory and put the following content into it.

```
Hello World
```

You should now be able to open the file through the url `https://localhost/hello.html`.

It can look like this.

![hello html](img/hello-html.png)

Ok, it seems to work.



Logging
--------------------------

Log files for services are usually stored below `/var/log`.

By default, the nginx access log is located at `/var/log/nginx/access.log`, verify that it contains the log entries you just made.

<!--
* show how it can look like
* make an example of the error log
-->


Reload and restart
--------------------------

Lets make a change in the configuration file and then reload it, without restarting the service. It should just reload its configuration.

We need to update the configuration file with the following entry. You can find this setting in the file `/etc/nginx/conf.d/default.conf`.

```
server {
    listen 9080;
}
```

Now make nginx to reload its configuration (use sudo).

```
systemctl reload nginx
```

Check the status of the service and you might find trace in the log that it was reloaded.

Verify that the service now listens on port 9080 using your web browser and the url `http://localhost:9080`

It can look like this.

![nginx port](img/nginx-port.png)

Doing reload is a nicer way than restarting the server. Reload just reloades the configuration and tries to keep the service upp all the time. Doing a restart might involve a short period of downtime.



Configure CGI
--------------------------

_Show how to configure cgi on nginx._



Summary
--------------------------

This was the process of installing, configuring, start, stop, restart and reload a service controller by systemctl.

You might have read in the manual for Nginx that all of these actions can be done through using the actual nginx software also. What systemctl provides is a friendly and unified way to access all services on the system.
