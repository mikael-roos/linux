---
revision:
    "2024-01-23": "(B, mos) Checked and docker build updated for shellcheck."
    "2023-01-05": "(A, mos) First version."
---
Execute the example programs in a Docker container
==========================

![work](img/docker-cowsay.png)

This shows how to clone the example repo and how to execute its example programs in a Docker container.

[[_TOC_]]


<!--
TODO

* shellcheck, unit tests with bat?
* recreate the main.bas and clean it up before using it?

-->



Precondition
--------------------------

You have installed Git and Docker with `docker-compose` on your system.

You are running on a bash terminal.

You have reviewed the slides "[Shell scripting with Bash](https://mikael-roos.gitlab.io/linux/lecture/shell-scripting/slide.html)".



Clone the example repo
--------------------------

Start by cloning [the example repo linux](https://gitlab.com/mikael-roos/linux).

```text
git clone https://gitlab.com/mikael-roos/linux.git
cd linux
```



Start the docker container
--------------------------

Start the docker container.

```text
docker-compose run shell bash
```



Run the example programs
--------------------------

You now have a docker container where you can execute the sample programs.

```text
cd sample/bash
./hello.bash
```



Run the linter
--------------------------

You can now run the linter.

```text
shellcheck hello.bash
shellcheck hello_full.bash
```

From the second command you migth see some messages from the linter. These should in general be corrected.



Create your own files
--------------------------

You can now create your own files in the root of the repo. Create a directory `me/` and add you own example code into it. 

The files you  create will be mounted into the container so you can execute them there and edit them outside of the container.

Use the container to execute the programs and run the linter.



Dockerfile
--------------------------

The container is setup as "shell" in the file [`docker-compose.yaml`](../../docker-compose.yaml).

The image is built from the file [`docker/shell/Dockerfile`](../../docker/shell/Dockerfile).



Add a tool to the image
--------------------------

Add the program `cowsay` to the image and rebuild it.

Start with the file [`docker/shell/Dockerfile`](../../docker/shell/Dockerfile) and add so that the tool/command `cowsay` is installed into the image.

Rebuild the image.

```text
docker-compose build shell
```

Start the newly rebuilt container.

```text
docker-compose run shell bash
```

Verify that the command `cowsay` was installed and that it works.

```text
whereis cowsay
man cowsay
cowsay "Hello world"
fortune | cowsay
```



Summary
--------------------------

You now have access to a development environment for bash scripts using Docker.
